<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
    'uses'   => 'NewShopController@index',
    'as'    =>  '/'
]);
Route::get('/category/product/{id}', [
    'uses'   => 'NewShopController@categoryProduct',
    'as'    =>  'category-product'
]);
Route::get('/mail-us', [
    'uses'  => 'NewShopController@mail',
    'as'    => '/mail-us'
]);
Route::get('/product/details/{id}', [
    'uses'  => 'NewShopController@detailProductView',
    'as'    => 'single-view'
]);

/*Cart related route*/
Route::post('/cart/add', [
    'uses'  => 'CartController@addToCart',
    'as'    => 'add-to-cart'
]);
Route::get('/cart/view', [
    'uses'  => 'CartController@viewCart',
    'as'    => 'view-cart'
]);
Route::get('/empty/cart', [
    'uses'  => 'CartController@emptyCart',
    'as'    => 'empty-cart'
]);
Route::post('/update/cart/item', [
    'uses'  => 'CartController@updateCartItem',
    'as'    => 'update-cart-item'
]);
Route::get('delete/cart/item/{id}', [
    'uses'  => 'CartController@deleteCartItem',
    'as'    => 'delete-cart-item'
]);


/*customer login registration route*/

Route::get('/customer/registration', [
    'uses'  => 'CustomerController@index',
    'as'    => 'customer-registration'
]);
Route::get('/customer/login', [
    'uses'  => 'CustomerController@customerLogin',
     'as'    => 'customer-login'
]);

Route::post('/new/customer', [
    'uses'  => 'CustomerController@addCustomer',
    'as'    => 'new-customer'
]);
Route::post('/verify/login', [
    'uses'  => 'CustomerController@verifyLogin',
    'as'    => 'verify-login'
]);
Route::get('/customer/logout', [
    'uses'  => 'CustomerController@logout',
    'as'    => 'customer-logout'
]);
/* checkout related route*/
Route::get('/checkout/shipping', [
    'uses'  => 'CheckoutController@getShippingInfo',
    'as'    => 'checkout-shipping'
]);

/*category related Route*/
Route::get('/add-category', [
    'uses'  => 'CategoryController@index',
    'as'    => 'add-category'
]);
Route::get('/manage-category', [
    'uses'  => 'CategoryController@getCategories',
    'as'    => 'manage-category'
]);
Route::post('/save-category', [
    'uses'  => 'CategoryController@saveCategory',
    'as'    => 'save-category'
]);

Route::get('/category/unpublished/{id}', [
   'uses'   => 'CategoryController@statusUnpublished',
    'as'    => 'category-unpublished'
]);
Route::get('/category/published/{id}', [
    'uses'   => 'CategoryController@statusPublished',
    'as'    => 'category-published'
]);
Route::get('/category/remove/{id}', [
   'uses'   => 'CategoryController@deleteCategory',
    'as'    => 'remove'
]);
Route::get('/category/view/{id}', [
    'uses'   => 'CategoryController@getCategoryById',
    'as'    => 'view'
]);
Route::post('/category/update', [
    'uses'  =>  'CategoryController@updateCategory',
    'as'    => 'update-category'
]);

Route::get('/brand/add', [
    'uses'  => 'BrandController@index',
    'as'    => 'add-brand'
]);
Route::post('/brand/save', [
    'uses'  => 'BrandController@store',
    'as'    => 'save-brand'
]);
Route::get('/brand/manage', [
    'uses'  => 'BrandController@getBrands',
    'as'    => 'manage-brand'
]);
Route::get('/brand/remove/{id}', [
    'uses'  => 'BrandController@destroy',
    'as'    => 'brand-remove'
]);
Route::get('/brand/published/{id}', [
   'uses'   => 'BrandController@statusPublished',
    'as'    => 'brand-published'
]);
Route::get('/brand/unpublished/{id}', [
    'uses'   => 'BrandController@statusUnpublished',
    'as'    => 'brand-unpublished'
]);
Route::get('/brand/view/{id}', [
    'uses'  => 'BrandController@show',
    'as'    => 'view-brand'
]);
Route::post('/brand/update', [
    'uses'   => 'BrandController@update',
    'as'    => 'update-brand'
]);

Route::get('/product/add', [
   'uses'   => 'ProductController@index',
    'as'    => 'add-product'
]);
Route::post('/product/save', [
   'uses'   => 'ProductController@saveProduct',
    'as'    => 'new-product'
]);
Route::get('/product/manage', [
   'uses'   => 'ProductController@manageProduct',
    'as'    => 'product-manage'
]);
Route::get('product/delete/{id}', [
    'uses'  => 'ProductController@deleteProduct',
    'as'    => 'delete-product'
]);
Route::get('/product/published/{id}', [
   'uses'   => 'ProductController@statusPublished',
    'as'    => 'status-published'
]);
Route::get('/product/unpublished/{id}', [
    'uses'   => 'ProductController@statusUnpublished',
    'as'    => 'status-unpublished'
]);
Route::get('/product/view/{id}', [
    'uses'  => 'ProductController@viewProduct',
    'as'    => 'view-product'
]);
Route::post('/product/update', [
   'uses'  => 'ProductController@updateProduct',
    'as'    => 'update-product'
]);
//Route::get('/login', [
//   'uses'   =>
//]);

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
