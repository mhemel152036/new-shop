@extends('front-end.master')
@section('body-content')
    <!--banner-->
    <div class="banner1">
        <div class="container">
            <h3><a href="{{ route('/') }}">Home</a> / <span>Cart</span></h3>
        </div>
    </div>
    <!--banner-->

    <!--content-->
    <div class="content">
        <div class="cart-items">
            <div class="container">
                <h3 class="text-center text-info">My Shoping Cart</h3>
                <hr>
                <table id="cart" class="table table-hover table-condensed">
                    <thead>
                    <tr>
                        <th style="width:50%">Product</th>
                        <th style="width:10%">Price</th>
                        <th style="width:8%">Quantity</th>
                        <th style="width:22%" class="text-center">Subtotal</th>
                        <th style="width:10%"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach(Cart::content() as $row)
                    <tr>
                        <td data-th="Product">
                            <div class="row">
                                <div class="col-sm-2 hidden-xs"><img src="{{ asset($row->options->image) }}" alt="..." class="img-responsive"/></div>
                                <div class="col-sm-10">
                                    <h4 class="nomargin">{{ $row->name }}</h4>
                                    <p>{{ $row->options->description }}</p>
                                </div>
                            </div>
                        </td>
                        <td data-th="Price">{{ $row->priceTotal }}</td>
                        <td data-th="Quantity">
                            {{ Form::open(['id' => 'qtyControlForm', 'route' => 'update-cart-item', 'method' => 'POST']) }}
                                <input type="number" class="form-control text-center" name="qty" value="{{ $row->qty }}" min="1"/>
                                <input type="hidden" name="rowId" value="{{ $row->rowId }}">
                            {{ Form::close() }}
                        </td>
                        <td data-th="Subtotal" class="text-center">{{ $row->subtotal }}</td>
                        <td class="actions" data-th="">
                            <a href="{{ route('view-cart') }}" onclick="event.preventDefault(); document.getElementById('qtyControlForm').submit();" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></a>
                            <a href="{{ route('delete-cart-item', ['id' => $row->rowId]) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr class="visible-xs">
                        <td class="text-center"><strong>Total 1.99</strong></td>
                    </tr>
                    <tr>
                        <td><a href="{{ route('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                        <td colspan="2" class="hidden-xs"></td>
                        <td class="hidden-xs text-center"><strong>Total: {{ Cart::total(0,'','') }} (+10% vat.)</strong></td>
                        <td>
                            @if(Session::get('customerId'))
                                <a {{ count(Cart::content()) == 0 ? 'disabled' : '' }} href="{{ route('checkout-shipping') }}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a>
                            @else
                                <a {{ count(Cart::content()) == 0 ? 'disabled' : '' }} href="{{ route('customer-registration') }}" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a>
                            @endif
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- checkout -->
    </div>
@endsection