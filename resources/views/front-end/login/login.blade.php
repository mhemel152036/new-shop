@extends('front-end.master')

@section('custom-css-content')
<style>
	.login-form {
		width: 450px;
		margin: 50px auto;
	}
	.login-form form {
		margin-bottom: 15px;
		background: #f7f7f7;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		padding: 30px;
	}
	.login-form h2 {
		margin: 0 0 15px;
	}
	.form-control, .btn {
		min-height: 38px;
		border-radius: 2px;
	}
	.btn {
		font-size: 15px;
		font-weight: bold;
	}
</style>
@endsection

@section('body-content')
	<div class="banner1">
		<div class="container">
			<h3><a href="{{ route('/') }}">Home</a> / <span>Login</span></h3>
		</div>
	</div>
	<div class="content">
		<div class="single-wl3">
			<div class="container">
				<div class="login-form">
					{{ Form::open(['route' => 'verify-login', 'method' => 'POST']) }}
						<h2 class="text-center">Log in</h2>
                        @if(Session::get('message'))
                            <p class="text-danger text-center">{{ Session::get('message') }}</p>
                        @endif
                        @if(Session::get('hint-message'))
                            <p class="text-warning">{{ Session::get('hint-message') }}</p>
                        @endif
                        <br>
						<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
							<input type="text" class="form-control" name="email" placeholder="Email" required>
							<small class="text-danger">{{ $errors->has('email') ? $errors->first('email') : '' }}</small>
						</div>
						<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
							<input type="password" class="form-control" name="password" placeholder="Password" required>
							<small class="text-danger">{{ $errors->has('password') ? $errors->first('password') : '' }}</small>
						</div>
						<div class="form-group">
							<input type="submit" name="btn" value="Sign In" class="btn btn-primary btn-block">
						</div>
						<div class="clearfix">
							<label class="pull-left checkbox-inline"><input type="checkbox"> Remember me</label>
							<a href="#" class="pull-right">Forgot Password?</a>
						</div>
					{{ Form::close() }}
					<p class="text-center"><a href="{{ route('customer-registration') }}">Create an Account</a></p>
				</div>
				</div>
			</div>
		</div>
	</div>
@endsection