@extends('front-end.master')

@section('body-content')
    <div class="banner1">
        <div class="container">
            <h3><a href="{{ route('/') }}">Home</a> / <span>Shipping</span></h3>
        </div>
    </div>
    <div class="content">
        <div class="single-wl3">
            <p class="text-center">Dear <strong>{{ Session::get('name') }}</strong>, you have to provide shipping information to complete your valuable order.</p>
        </div>
    </div>
    <div class="content">
        <div class="single-wl3">
            <div class="container">
                <div class="row ">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-6 well">
                        <h3 class="text-center">Shipping Form</h3>
                        <br>
                        {{ Form::open() }}
                            <div class="form-group row">
                                <lable class="col-sm-2">Name</lable>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="name" placeholder="Name"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <lable class="col-sm-2">Email</lable>
                                <div class="col-sm-10">
                                    <input class="form-control" type="email" name="email" placeholder="Email"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <lable class="col-sm-2">Phone</lable>
                                <div class="col-sm-10">
                                    <input class="form-control" type="text" name="phone" placeholder="Phone"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <lable class="col-sm-2">Address</lable>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="address" id="address" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10"><input class="btn btn-success" type="submit" name="btn" value="Submit Info"></div>
                            </div>

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection