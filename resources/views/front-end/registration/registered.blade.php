@extends('front-end.master')
@section('custom-css-content')
<style>
	body {
		color: black;
		background: #FFFFFF;
		font-family: 'Roboto', sans-serif;
	}
	.form-control{
		height: 40px;
		box-shadow: none;
		color: #969fa4;
	}
	.form-control:focus{
		border-color: #5cb85c;
	}
	.form-control, .btn{
		border-radius: 3px;
	}
	.signup-form{
		width: 550px;
		margin: 0 auto;
		padding: 30px 0;
	}
	.signup-form h2{
		color: #636363;
		margin: 0 0 15px;
		position: relative;
		text-align: center;
	}
	.signup-form h2:before, .signup-form h2:after{
		content: "";
		height: 2px;
		width: 30%;
		background: #d4d4d4;
		position: absolute;
		top: 50%;
		z-index: 2;
	}
	.signup-form h2:before{
		left: 0;
	}
	.signup-form h2:after{
		right: 0;
	}
	.signup-form .hint-text{
		color: #999;
		margin-bottom: 30px;
		text-align: center;
	}
	.signup-form form{
		color: #999;
		border-radius: 3px;
		margin-bottom: 15px;
		background: #f2f3f7;
		box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
		padding: 30px;
	}
	.signup-form .form-group{
		margin-bottom: 20px;
	}
	.signup-form input[type="checkbox"]{
		margin-top: 3px;
	}
	.signup-form .btn{
		font-size: 16px;
		font-weight: bold;
		min-width: 140px;
		outline: none !important;
	}
	.signup-form .row div:first-child{
		padding-right: 10px;
	}
	.signup-form .row div:last-child{
		padding-left: 10px;
	}
	.signup-form a{
		color: grey;
        font-weight: bold;
		text-decoration: none;
	}
	.signup-form a:hover{
		text-decoration: none;
	}
	.signup-form form a{
		color: #5cb85c;
		text-decoration: none;
	}
	.signup-form form a:hover{
		text-decoration: underline;
	}
</style>
@endsection
@section('body-content')
    <div class="banner1">
        <div class="container">
            <h3><a href="{{ route('/') }}">Home</a> / <span>Registered</span></h3>
        </div>
    </div>
		<!--banner-->
		<div class="content">
			<div class="single-wl2">
				<div class="container">
					<div class="row">
							<div class="signup-form">
								{{ Form::open(['route' => 'new-customer', 'method' => 'POST']) }}
									<h2>Register</h2>
									<p class="hint-text">You must have to be Registered to complete your Order! <br> Create your account.</p>
									<p class="text-success">{{ Session::get('message') }}</p><br>
									<div class="form-group {{ ($errors->has('first_name') or $errors->has('last_name')) ? 'has-error' : "" }}">
										<div class="row">
											<div class="col-xs-6"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
											<div class="col-xs-6"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
											<small class="text-danger">{{ $errors->has('first_name') ? $errors->first('first_name') : "" }}</small>
											<small class="text-danger">{{ $errors->has('last_name') ? $errors->first('last_name') : "" }}</small>
										</div>
									</div>
									<div class="form-group {{ $errors->has('email') ? 'has-error' : "" }}">
										<input type="email" class="form-control" name="email" placeholder="Email" required="required">
										<small class="text-danger">{{ $errors->has('email') ? $errors->first('email') : "" }}</small>
									</div>
									<div class="form-group {{ $errors->has('password') ? 'has-error' : "" }}">
										<input type="password" class="form-control" name="password" placeholder="Password" required="required">
										<small class="text-danger">{{ $errors->has('password') ? $errors->first('password') : "" }}</small>
									</div>
									<div class="form-group">
										<input type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required="required">
									</div>
									<div class="form-group {{ $errors->has('phone') ? 'has-error' : "" }}">
										<input type="text" class="form-control" name="phone" placeholder="Phone" required="required">
										<small class="text-danger">{{ $errors->has('phone') ? $errors->first('phone') : "" }}</small>
									</div>
									<div class="form-group {{ $errors->has('address') ? 'has-error' : "" }}">
										<textarea name="address" class="form-control" rows="3" cols="30" placeholder="Address" required></textarea>
										<small class="text-danger">{{ $errors->has('address') ? $errors->first('address') : "" }}</small>
									</div>
									<div class="form-group">
										<label class="checkbox-inline"><input type="checkbox" required="required"> I accept the <a href="#">Terms of Use</a> &amp; <a href="#">Privacy Policy</a></label>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-success btn-lg btn-block">Register Now</button>
									</div>
								{{ Form::close() }}
								<div class="text-center">Already have an account? <a href="{{ route('customer-login') }}">Sign in</a></div>
							</div>
						</div>
				</div>
			</div>
		</div>
@endsection