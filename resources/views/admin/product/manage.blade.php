@extends('admin.master')

@section('title')
    Manage-products
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Manage Brand</h3>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Mange product</h3>
        </div>
        <div class="panel-body">
            @if(Session::get('message')!=null)
            <div class="alert alert-success alert-dismissible">
                <button class="close" data-dismiss="alert">&times;</button>
                <strong class="text-light">{{ Session::get('message') }}</strong>
            </div>
            @endif
            <table class="table table-bordered">
                <thead class="bg-primary">
                <tr>
                    <td>#</td>
                    <td>Cate. name</td>
                    <td>Brand name</td>
                    <td>Product name</td>
                    <td>Quantity</td>
                    <td>Price</td>
                    <td>Short description</td>
                    {{--<td>Long description</td>--}}
                    <td>Post</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                @php($i=1)
                @foreach($products as $product)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ $product->category_name }}</td>
                        <td>{{ $product->brand_name }}</td>
                        <td>{{ $product->product_name }}</td>
                        <td>{{ $product->product_quantity }}</td>
                        <td>{{ $product->product_price }}</td>
                        <td>{{ $product->short_description }}</td>
                        {{--<td>{{ $product->long_description }}</td>--}}
                        <td>{{ $product->publication_status ? 'Published' : 'Unpublished' }}</td>
                        <td>
                            @if($product->publication_status)
                            <a class="btn btn-primary btn-xs" href="{{ route('status-unpublished',['id' => $product->id]) }}"><span class="glyphicon glyphicon-arrow-down"></span></a>
                            @else
                            <a class="btn btn-info btn-xs" href="{{ route('status-published', ['id' => $product->id]) }}"><span class="glyphicon glyphicon-arrow-up"></span></a>
                            @endif
                            <a class="btn btn-warning btn-xs" href="{{ route('view-product', ['id' => $product->id]) }}"><span class="glyphicon glyphicon-edit"></span></a>
                            <a class="btn btn-danger btn-xs" href="{{ route('delete-product',['id'=>$product->id]) }}" onclick="return confirm('Are you sure to delete product info?')"><span class="glyphicon glyphicon-trash"></span></a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection