@extends('admin.master')

@section('title')
    Add Product
@endsection

@section('main-content')
    <div class="row ">
        <div class="col-lg-12">
            <h3 class="page-header">Add Product</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1"></div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Add Product</h4>
                </div>
                <div class="panel-body">
                    @if(Session::get('message')!=null)
                        <div class="alert alert-success alert-dismissible">
                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif

                    {{--<form action="{{ route('new-product') }}" method="POST" enctype="multipart/form-data">--}}
                        {{--@csrf--}}

                        {{ Form::open(['route' => 'new-product', 'method' => 'POST', 'files' => true]) }}
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-2" for="category-id">Category Name</label>
                            <div class="col-sm-12 col-lg-9 col-xl-10">
                                <select class="form-control" name="category_id" id="category-id">
                                    <option value="">--Select Category Name--</option>
                                        @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                        @endforeach
                                </select>
                                <small class="text-danger">{{ $errors->has('category_id') ? $errors->first('category_id') : '' }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-3" for="brand-id">Brand Name</label>
                            <div class="col-sm-12 col-lg-9 col-xl-9">
                                <select class="form-control" name="brand_id" id="brand-id">
                                    <option value="">--Select Brand Name--</option>
                                        @foreach($brands as $brand)
                                        <option value="{{ $brand->id }}">{{ $brand->brand_name }}</option>
                                        @endforeach
                                </select>
                                <small class="text-danger">{{ $errors->has('brand_id') ? $errors->first('brand_id') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-3" for="product-name">Product Name</label>
                            <div class="col-sm-12 col-lg-9 col-xl-9">
                                <input class="form-control" type="text" name="product_name" id="product-name"/>
                                <small class="text-danger">{{ $errors->has('product_name') ? $errors->first('product_name') : '' }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-3" for="quantity">Product Quantity</label>
                            <div class="col-sm-12 col-lg-9 col-xl-9">
                                <input class="form-control" type="text" name="product_quantity" id="quantity"/>
                                <small class="text-danger">{{ $errors->has('quantity') ? $errors->first('quantity') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-3" for="price">Product Price</label>
                            <div class="col-sm-12 col-lg-9 col-xl-9">
                                <input class="form-control" type="number" name="product_price" id="price"/>
                                <small class="text-danger">{{ $errors->has('price') ? $errors->first('price') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-2" for="short-description">Short Description</label>
                            <div class="col-sm-12 col-lg-9 col-xl-10">
                                <textarea class="form-control" name="short_description" id="short-description" rows="2" cols="10"></textarea>
                                <small class="text-danger">{{ $errors->has('short_description') ? $errors->first('short_description') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-2" for="long-description">Long Description</label>
                            <div class="col-sm-12 col-lg-9 col-xl-10" >
                                <textarea id="editor" class="form-control" name="long_description" id="long-description" rows="8" cols="10"></textarea>
                                <small class="text-danger">{{ $errors->has('long_description') ? $errors->first('long_description') : '' }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-2" for="product-img">Product Image</label>
                            <div class="col-sm-12 col-lg-9 col-xl-10">
                                <input class="form-control-file" type="file" name="product_img" id="product-img"/>
                                <small class="text-danger">{{ $errors->has('product_img') ? $errors->first('product_img') : '' }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-3 col-xl-3">Publication Status</label>
                            <div class="col-sm-12 col-lg-9 col-xl-9 form-check">
                                <label class="form-check-label"><input type="radio" name="publication_status" value="1" checked /> Yes</label>
                                <label class="form-check-label"><input type="radio" name="publication_status" value="0"/> No</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-lg-3 col-xl-3"></div>
                            <div class="col-sm-12 col-lg-9 col-xl-9">
                                <input class="btn btn-success btn-block" type="submit" name="btn" value="Add-Product"/>
                            </div>
                        </div>
                    {{--</form>--}}
                        {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
@endsection