@extends('admin.master')

@section('title')
    View Brand
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Update Brand</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Update Brand</h4>
                </div>
                <div class="panel-body">
                    <form action="{{ route('update-brand') }}" method="POST">
                        @csrf
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="brand-id">Brand Id</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="form-control" type="text" name="brand_id" id="brand-id" value="{{ $brand->id }} " readonly/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="brand-name">Brand Name</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="form-control" type="text" name="brand_name" id="brand-name" value="{{ $brand->brand_name }}"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="brand-description">Brand Description</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <textarea class="form-control" name="brand_description" id="brand-description" cols="30" rows="4">{{ $brand->brand_description }}</textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3">Publication status</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9 form-check">
                                <label class="form-check-label"><input class="m-5" type="radio" name="publication_status" value="1" checked> Published</label>
                                @if(!$brand->publication_status)
                                    <label class="form-check-label"><input type="radio" name="publication_status" value="0" checked> Unpublished</label>
                                @else
                                    <label class="form-check-label"><input type="radio" name="publication_status" value="0" checked> Unpublished</label>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-lg-4 col-xl-3"></div>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="btn btn-success btn-block" type="submit" name="btn" value="Update-brand">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection