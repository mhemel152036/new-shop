@extends('admin.master')

@section('title')
    Add Brand
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Add Brand</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Add Brand</h4>
                </div>
                <div class="panel-body">
                    @if(Session::get('message')!=null)
                        <div class="alert alert-success alert-dismissible">
                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif

                    <form action="{{ route('save-brand') }}" method="POST">
                        @csrf
                        <div class="row form-group {{ $errors->has('brand_name') ? 'has-error' : '' }}">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="brand-name">Brand Name</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="form-control" type="text" name="brand_name" id="brand-name"/>
                                <small class="text-danger">{{ $errors->has('brand_name') ? $errors->first('brand_name') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group {{ $errors->has('brand_name') ? 'has-error' : '' }}">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="brand-description">Brand Description</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <textarea class="form-control" name="brand_description" id="brand-description" cols="30" rows="4"></textarea>
                                <small class="text-danger">{{ $errors->has('brand_name') ? $errors->first('brand_name') : "" }}</small>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3">Publication status</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9 form-check">
                                <label class="form-check-label"><input class="m-5" type="radio" name="publication_status" value="1" checked> Published</label>
                                <label class="form-check-label"><input type="radio" name="publication_status" value="0"> Unpublished</label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-lg-4 col-xl-3"></div>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="btn btn-success btn-block" type="submit" name="btn" value="Add-brand">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection