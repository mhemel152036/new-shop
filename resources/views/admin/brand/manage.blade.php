@extends('admin.master')

@section('title')
    Manage Brand
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Manage Brand</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1">

        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Brands</h3>
                </div>
                <div class="panel-body">
                    @if(Session::get('message'))
                        <div class="row">
                            <div class="alert alert-success alert-dismissible">
                                <button class="close" data-dismiss="alert"><span>&times;</span></button>
                                <strong>{{ Session::get('message') }}</strong>
                            </div>
                        </div>
                    @endif
    <table class="table table-bordered table-hover table-striped">
        <thead class="bg-primary">
            <tr>
                <th>#</th>
                <th>Brand Name</th>
                <th>Brand Description</th>
                <th>Publication Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @php($i=1)
            @foreach($brands as $brand)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $brand->brand_name }}</td>
                <td>{{ $brand->brand_description }}</td>
                <td>{{ $brand->publication_status ? 'Published' : 'Unpublished' }}</td>
                <td>
                    @if($brand->publication_status)
                    <a href="{{ route('brand-unpublished',['id' => $brand->id]) }}" class="btn btn-xs btn-primary"> <span class="glyphicon glyphicon-arrow-down"></span></a>
                    @else
                    <a href="{{ route('brand-published',['id' => $brand->id]) }}" class="btn btn-xs btn-success"> <span class="glyphicon glyphicon-arrow-up"></span></a>
                    @endif

                    <a href="{{ route('view-brand',['id' => $brand->id ]) }}" class="btn btn-xs btn-warning"> <span class="glyphicon glyphicon-edit"></span></a>
                    <a href="{{ route('brand-remove',['id'  => $brand->id]) }}" onclick="return confirm('Are you sure to delete brans?')" class="btn btn-xs btn-danger"> <span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
                </div>
            </div>
        </div>
    </div>

@endsection