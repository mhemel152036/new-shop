<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Admin Login</title>
        <!-- Core CSS - Include with every page -->
        <link href="{{ asset('/') }}admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="{{ asset('/') }}admin/font-awesome/css/font-awesome.css" rel="stylesheet">
        <!-- SB Admin CSS - Include with every page -->
        <link href="{{ asset('/') }}admin/css/sb-admin.css" rel="stylesheet">
    </head>
    <body>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Admin Login Panel</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" method="POST" action="{{ route('login') }}">
                                @csrf
                                <fieldset>
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }} row">
                                        <label class="col-sm-3" for="email">E-mail</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" placeholder="E-mail" id="email" name="email" type="email" autofocus>
                                            @if($errors->has('email'))
                                                <span class="help-block">
                                                    <small>{{ $errors->first('email') }}</small>
                                                </span>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="form-group row {{ $errors->has('password') ? 'has-error' : '' }}">
                                        <label class="col-sm-3" for="password">Password</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" placeholder="Password" id="password" name="password" type="password" value="">
                                            @if($errors->has('password'))
                                                <span class="help-block">
                                                    <small>
                                                        {{ $errors->first('password') }}
                                                    </small>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="checkbox row">
                                        <div class="col-sm-3"></div>
                                        <label class="col-sm-9">
                                            <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                        </label>
                                    </div>
                                    <!-- Change this to a button or input when using this as a form -->
                                    <div class="form-group row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="submit" class="btn  btn-success btn-block" value="Login">
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Core Scripts - Include with every page -->
        <script src="{{ asset('/') }}admin/js/jquery-1.10.2.js"></script>
        <script src="{{ asset('/') }}admin/js/bootstrap.min.js"></script>
        <script src="{{ asset('/') }}admin/js/plugins/metisMenu/jquery.metisMenu.js"></script>
        <!-- SB Admin Scripts - Include with every page -->
        <script src="{{ asset('/') }}admin/js/sb-admin.js"></script>
    </body>
</html>
