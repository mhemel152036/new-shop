@extends('admin.master')

@section('title')
    Update category
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Manage Category</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Update category form</h4>
                </div>
                <div class="panel-body">
                    @if(Session::get('message')!=null)
                        <div class="alert alert-success alert-dismissible">
                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    @endif

                    <form action="{{ route('update-category') }}" method="POST">
                        @csrf
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="category-name">Category Id</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="form-control" type="text" name="category_id" id="category-id" value="{{ $category->id }}" readonly />
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="category-name">Category Name</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="form-control" type="text" name="category_name" id="category-name" value="{{ $category->category_name }}"/>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3" for="category-description">Category Description</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <textarea class="form-control" name="category_description" id="category-description" cols="30" rows="4">{{ $category->category_description }}</textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-sm-12 col-lg-4 col-xl-3">Publication status</label>
                            <div class="col-sm-12 col-lg-8 col-xl-9 form-check">
                                <label class="form-check-label"><input class="m-5" type="radio" name="publication_status" value="1" checked> Published</label>
                                @if(!$category->publication_status)
                                    <label class="form-check-label"><input type="radio" name="publication_status" value="0" checked> Unpublished</label>
                                @else
                                    <label class="form-check-label"><input type="radio" name="publication_status" value="0"> Unpublished</label>
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-12 col-lg-4 col-xl-3"></div>
                            <div class="col-sm-12 col-lg-8 col-xl-9">
                                <input class="btn btn-success btn-block" type="submit" name="btn" value="Update-Category">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection