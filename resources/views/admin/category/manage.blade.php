@extends('admin.master')

@section('title')
    Manage  Category
@endsection

@section('main-content')
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">Manage Category</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-1">

        </div>
        <div class="col-sm-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Categories</h3>
                </div>
                <div class="panel-body">
                    @if(Session::get('message'))
                    <div class="row">
                        <div class="alert alert-success alert-dismissible">
                            <button class="close" data-dismiss="alert"><span>&times;</span></button>
                            <strong>{{ Session::get('message') }}</strong>
                        </div>
                    </div>
                    @endif

                    <table class="table table-bordered">
                        <thead class="bg-primary">
                        <tr>
                            <th>#</th>
                            <th>Category Name</th>
                            <th>Category Description</th>
                            <th>Publication Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php($i=1)
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $category->category_name }}</td>
                                <td>{{ $category->category_description }}</td>
                                <td>{{ $category->publication_status ? 'Published' : 'Unpublished' }}</td>
                                <td>
                                    @if($category['publication_status'])
                                        <a href="{{ route('category-unpublished',['id' => $category->id]) }}" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-arrow-down"></span></a>
                                    @else
                                        <a href="{{ route('category-published', ['id' => $category->id ]) }}" class="btn btn-xs btn-success"><span class="glyphicon glyphicon-arrow-up"></span></a>
                                    @endif
                                    <a href="{{ route('view',['id' => $category->id]) }}" class="btn btn-xs btn-warning"><span class="glyphicon glyphicon-edit"></span></a>
                                    <a href="{{ route('remove', ['id' => $category->id ]) }}" onclick="return confirm('Are you sure to delete category?'); " class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection