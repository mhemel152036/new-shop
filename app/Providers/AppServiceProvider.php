<?php

namespace App\Providers;
use App\Brand;
use Illuminate\Support\ServiceProvider;
use App\Category;
use View;
use App\Product;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        View::share('categories', Category::where('publication_status', 1) ->get());
        View::composer('*', function ($view) {
            $brands = Brand::where('publication_status', 1)
                ->take(25)
                ->get();
            $view->with('categories', Category::where('publication_status', 1) ->get());
            $view->with('brands', $brands);
        });
        View::composer('*', function ($view) {
            $newProduct = Product::where('publication_status', 1)
                ->orderBy('id', 'DESC')
                ->take(8)
                ->skip(4)
                ->get();
            $newArrival = Product::where('publication_status', 1)
                ->orderBy('id','DESC')
                ->take(4)
                ->get();
            $view->with('newArrival', $newArrival);
            $view->with('newProduct', $newProduct);
        });
    }
}
