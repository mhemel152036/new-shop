<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index() {
        return view('admin.category.add');
    }
    protected function validateCategory(Request $request) {
        $request->validate([
            'category_name'         => 'required|string||max:150',
            'category_description'  => 'required',
            'publication_status'    => 'required',
        ]);
    }
    public function saveCategory(Request $request) {
        $this->validateCategory($request);

        $category = new Category();
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;
        $category->publication_status = $request->publication_status;
        $category->save();

        return redirect('/add-category')->with('message', "Category info added successfully!");
    }
    public function getCategories() {
        $categories = Category::all();
        return view('admin.category.manage', [
            'categories'    => $categories
        ]);
    }
    public function statusUnpublished($id) {
        $category=Category::find($id);
        $category->publication_status = 0;
        $category->save();

        return redirect('/manage-category') ->with('message', 'Publication status is changed to unpublished!');
    }
    public function statusPublished($id) {
        $category = Category::find($id);
        $category->publication_status = 1;
        $category->save();

        return redirect('/manage-category') ->with('message', 'Publication status is changed to Published!');
    }
    public function deleteCategory($id) {
        $category = Category::find($id);
        $category->delete();

        return redirect('/manage-category')->with('message', "Category deleted successfully!");
    }
    public function getCategoryById($id) {
        $category = Category::find($id);
        return view('admin.category.update', [
            'category'  => $category
        ]);
    }
    public function updateCategory(Request $request) {
        $category = Category::find($request->category_id);
        $category->category_name = $request->category_name;
        $category->category_description = $request->category_description;
        $category->publication_status = $request->publication_status;
        $category->save();

        return redirect('/manage-category')->with('message', 'category information update successfully!');
    }
}
