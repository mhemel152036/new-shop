<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Http\Request;

class NewShopController extends Controller
{
    public function index() {
        $likeAbleProduct = Product::where('category_id','>',2)
                            ->where('publication_status', 1)
                            ->take(4)
                            ->get();

        return view('front-end.home.home',[
            'likeAbleProduct'   => $likeAbleProduct
        ]);
    }
    public function categoryProduct($id) {
        $category = Category::find($id);
        $products = Product::where('category_id', $id)
                            ->where('publication_status', 1)
                            ->get();
        return view('front-end.category-products.products', [
            'category_name'    => $category->category_name,
            'products'  => $products
        ]);
    }
    public function detailProductView($id) {
        $product = Product::find($id);
        $similarProducts = Product::where('category_id', $product->category_id)
                                    -> where('publication_status', 1)
                                    -> take(4)
                                    -> get();
        return view('front-end.product-details.single', [
            'product'           => $product,
            'similarProducts'   => $similarProducts
        ]);
    }

    public function mail() {
        return view('front-end.mail.mail');
    }
}
