<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
//use Intervention\Image\Facades\Image;
use Image;
use DB;
class ProductController extends Controller
{
    public function index() {
        $brands = Brand::where('publication_status', 1)->get();
        $categories = Category::where('publication_status', 1)->get();
        return view('admin.product.add')
                    -> with('brands', $brands)
                    -> with('categories', $categories);
    }
    public function saveProduct(Request $request) {
        $this->validateProduct($request);
        $imgUrl = $this->imgUpload($request);
        $this->addProductToDb($request, $imgUrl);
        return redirect('/product/add')->with('message', 'Product saved successfully into the Database!');

    }
    protected function addProductToDb(Request $request, $imgUrl) {
        $product = new Product();
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_quantity = $request->product_quantity;
        $product->product_price = $request->product_price;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->product_img = $imgUrl;
        $product->publication_status = $request->publication_status;
        $product->save();
    }
    protected function imgUpload(Request $request) {
        $productImage = $request->file('product_img');
        $dir = 'admin/product-images/';
        $imgName = $productImage->getClientOriginalName();
        $imgUrl = $dir.$imgName;
//        conventional approach to upload img
//        $productImage->move($dir, $imgName);
        $image = Image::make($productImage);
		$image->resize(255,255);
        $image->save($imgUrl);

        return $imgUrl;
    }
    protected function validateProduct(Request $request){
        $request->validate([
            'category_id'           => 'required',
            'brand_id'              => 'required',
            'product_name'          => 'required|string|max:255',
            'product_quantity'      => 'required|numeric',
            'product_price'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'short_description'     => 'required|string',
            'long_description'      => 'required|string',
            'product_img'           => 'required|image|mimes:jpeg,jpg|max:2048',
            'publication_status'    => 'required|numeric'
        ]);
    }
    public function manageProduct() {
        $products = DB::table('products')
                        ->join('categories','products.category_id', '=', 'categories.id')
                        ->join('brands', 'products.brand_id', '=', 'brands.id')
                        ->select('products.*', 'brands.brand_name', 'categories.category_name')
                        ->get();
        return view('admin.product.manage', [
            'products'  => $products
        ]);
    }
    public function  deleteProduct($id) {
        $product = Product::find($id);
        $product->delete();
		return redirect('/product/manage') -> with('message', 'Product information deleted successfully');
    }
	public function statusUnpublished($id) {
		$product = Product::find($id);
		$product->publication_status = 0;
		$product->save();
		return redirect('/product/manage') -> with('message', 'Publication status is changed to Unpublished');
	}
	public function statusPublished($id) {
		$product = Product::find($id);
		$product->publication_status = 1;
        $product->save();
		return redirect('/product/manage') -> with('message', 'Publication status is changed to Published');
	}
	public function viewProduct($id) {
        $product = Product::find($id);
        $categories = Category::all();
        $brands = Brand::all();
        return view('admin.product.update')
                    -> with('product', $product)
                    -> with('categories', $categories)
                    -> with('brands', $brands);
    }
    public function updateProduct(Request $request) {
        $this->validateUpdateProduct($request);
        if ($request->file('product_img')) {
            $imgUrl = $this->imgUpload($request);
            $this->addUpdatedProductWithImg($request, $imgUrl);
        }else {
            $this->addUpdatedProduct($request);
        }
        return redirect('/product/manage') -> with('message', 'Product information updated successfully!');
    }
    protected function validateUpdateProduct(Request $request) {
        $request->validate([
            'category_id'           => 'required',
            'brand_id'              => 'required',
            'product_name'          => 'required|string|max:255',
            'product_quantity'      => 'required|numeric',
            'product_price'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'short_description'     => 'required|string',
            'long_description'      => 'required|string',
            'product_img'           => 'image|mimes:jpeg,jpg|max:2048',
            'publication_status'    => 'required|numeric'
        ]);
    }
    protected function addUpdatedProductWithImg(Request $request, $imgUrl) {
        $product = Product::find($request->id);
        /*delete old img stored into local database*/
        $this->removeOldImg($product->product_img);
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_quantity = $request->product_quantity;
        $product->product_price = $request->product_price;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->product_img = $imgUrl;
        $product->publication_status = $request->publication_status;
        $product->save();
    }
    protected function removeOldImg($imgUrl) {
        unlink($imgUrl);
    }
    protected function addUpdatedProduct(Request $request) {
        $product = Product::find($request->id);
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->product_name = $request->product_name;
        $product->product_quantity = $request->product_quantity;
        $product->product_price = $request->product_price;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->publication_status = $request->publication_status;
        $product->save();
    }

}
