<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Customer;
use Mail;
use Cart;
class CustomerController extends Controller
{
    public function index() {
        if(Session::get('customerId')) {
            return redirect('/');
        }
        return view('front-end.registration.registered');
    }
    public function customerLogin() {
        if(Session::get('customerId')) {
            return redirect('/');
        }
        return view('front-end.login.login');
    }
    public function addCustomer(Request $request) {
        $this->validateCustomer($request);
        $customer = $this->saveCustomerInfoIntoDb($request);

        Session::put('customerId', $customer->id);
        Session::put('name', $customer->first_name.' '.$customer->last_name);

//        $data= $customer->toArray();
//        Mail::send('front-end.mail.mail', $data, function ($message) use ($data){
//            $message->to($data['email']);
//            $message->subject('Welcome mail');
//        });
        if(count(Cart::content())) {
            return redirect('/checkout/shipping');
        }else{
            return redirect('/customer/registration')->with('message', '* Registration successful, please continue your shopping');
        }
    }
    protected function saveCustomerInfoIntoDb(Request $request) {
        $customer = new Customer();
        $customer->first_name = $request->first_name;
        $customer->last_name  = $request->last_name;
        $customer->email      = $request->email;
        $customer->password   = md5($request->password);
        $customer->phone      = $request->phone;
        $customer->address    = $request->address;
        $customer->save();
        return $customer;
    }
    protected function validateCustomer(Request $request) {
        $request->validate([
            'first_name'            => 'required|string|max:255|min:3',
            'last_name'             => 'required|string|max:255',
            'email'                 => 'required|unique:customers|email|max:255',
            'phone'                 => 'required|max:11',
            'password'              => 'required|confirmed|between:6,255',
            'password_confirmation' => 'required|same:password',
            'address'               => 'required'
        ]);
    }
    public function verifyLogin(Request $request) {
        $this->validateLogin($request);
        $email      = $request->email;
        $password   = md5($request->password);
        $customer = Customer::where('email', $email)
            ->where('password', $password)
            ->first();
        if($customer){
            Session::put('customerId', $customer->id);
            Session::put('name', $customer->first_name.' '.$customer->last_name);
            if(count(Cart::content())){
                return redirect('/checkout/shipping');
            }else{
                return redirect('/customer/login')->with('hint-message', 'Login successful but your cart is empty!');
            }
        }
        else{
            return redirect('/customer/login')->with('message', '* Invalid login, wrong email or password');
        }
    }
    protected function validateLogin(Request $request) {
        $request->validate([
            'email'     => 'required|email|max:255',
            'password'  => 'required|between:6,255'
        ]);
    }
    public function logout() {
        Session::forget('customerId');
        Session::forget('name');
        return redirect('/');
    }
}
