<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Cart;

class CartController extends Controller
{
    public function viewCart() {
        return view('front-end.cart.cart');
    }
    public function addToCart(Request $request) {
        $product = Product::find($request->id);
        Cart::add([
            'id' => $request->id,
            'name' => $product->product_name,
            'qty' => $request->qty,
            'price' => $product->product_price,
            'weight' => 550,
            'options' => [
                'description'   => $product->short_description,
                'image' => $product->product_img
            ]
        ]);
        Cart::setGlobalTax(10);
        return redirect('/cart/view');
    }
    public function deleteCartItem($rowId) {
        Cart::remove($rowId);
        return redirect('/cart/view');
    }
    public function emptyCart() {
        Cart::destroy();
        return redirect('/cart/view');
    }
    public function updateCartItem(Request $request) {
        $rowId = $request->rowId;
        Cart::update($rowId, ['qty' => $request->qty]);
        return redirect('/cart/view');
    }
}
