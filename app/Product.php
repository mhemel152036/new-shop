<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['brand_id', 'category_id', 'product_name', 'product_quantity', 'product_price', 'short_description', 'long_description', 'product_img', 'publication_status'];
}
